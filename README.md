#### Installation

1.   OpenJDK 11
2.   Intellij IDEA
3.   Gradle
4.   Docker + Docker compose
5.   UI Client    

##### Configuration

1. OpenJDK 11 (LTS)

* Download  
  https://adoptopenjdk.net/
  
* Configuration
    ```
    $ export JAVA_HOME=/path/to/java_home
    $ export PATH=$JAVA_HOME/bin:$PATH
    $ java --version
    ```
    
2.Intellij IDEA

* Download  
  https://www.jetbrains.com/idea/download

* Configuration     
  Increase memory heap  
  https://www.jetbrains.com/help/idea/increasing-memory-heap.html
  
  Enable the memory indicator      
  https://www.jetbrains.com/help/idea/increasing-memory-heap.html


3.Gradle
* Installation  
  https://gradle.org/install/

* Configuration
    ```
    $ export GRADLE_HOME=/path/to/gradle_home
    $ export PATH=$GRADLE_HOME/bin:$PATH
    $ gradle -version
    ```
* Gradle wrapper             
   It is also possible to use **Gradle Wrapper** directly without any installation.      
  `$ ./gradlew clean build`   
  `$ ./gradlew test`    
  `$ ./gradlew bootRun`     

4.Docker & Docker compose

* Installation     
  https://docs.docker.com/get-docker/
  https://docs.docker.com/compose/install/
    
5.GUI Clients
     
5.1.Postgres    
  * PostgreSQL client            
    https://postgresapp.com/documentation/gui-tools.html
  
5.2.Ldap    
  * Apache Directory Studio     
  http://directory.apache.org/studio/

           
#### Project:
##### Project structure

```
src
├── main
│   ├── java
│   │   └── com
│   │       └── orpea
│   │           └── ms
│   │               ├── common
│   │               ├── config
│   │               ├── controller
│   │               ├── domain
│   │               ├── model
│   │               ├── repository
│   │               ├── security
│   │               └── service
│   └── resources
│       ├── liquibase
│       │   ├── data
│       │   └── schema
│       ├── static
│       └── templates
└── test
    ├── java
    │   └── com
    │       └── orpea
    │           └── ms
    │               ├── common
    │               ├── controller
    │               └── service
    └── resources

```

##### Run application

* Using Gradle Wrapper (./gradlew) command line

```
$ cd ${LOCAL_REPOSITORY_HOME}/ms
$ ./gradlew bootRun
```

* Using IntelliJ    
  Simply add a new gradle configuration :    
  * Select **Run** | **Edit Configurations** from the **Main Menu**.           
  * In the Run/Debug Configurations dialog, click **add icon** and select **Gradle** to add a new configuration.
  * On the right side of the Run/Debug Configurations dialog, specify the following settings:    
    - In field **Name:**, enter 'MS bootRun'        
    - In field **Gradle project:**, choose 'ms' project from gradle projects list 
    - In field **Tasks:**, specify `bootRun` task 
  


##### Project Features      
Refer to the <a href="https://gitlab.limengo.net/orpea/ms-parent/-/tree/develop/docs/specification/technical" target="_blank">technical documents</a>

#### Tools     
* GitLab Remote Repository    
  https://gitlab.limengo.net/orpea/ms

* GitLab CI  
  https://gitlab.limengo.net/orpea/ms/pipelines
    
* Nexus     
  https://repo.limengo.net
 
* SonarQube    
  https://sonarqube.limengo.net

* Container Registry     
  https://gitlab.limengo.net/orpea/ms/container_registry    
    
#### MS API endpoints
    
* Dev   
  https://ms-dev.limengo.net 
* Test     
  https://ms-test.limengo.net
* Prd     
  https://ms.limengo.net

#### Swagger UI
    
* Local  
  http://localhost:8080/swagger-ui.html  
* Dev   
  https://ms-dev.limengo.net/swagger-ui.html 
* Test  
  https://ms-test.limengo.net/swagger-ui.html


#### Development mode 
1. **IntelliJ**         
    1.1 Set Gradle home path in IntelliJ    

    IntelliJ IDEA -> Preferences -> Build,Execution,Deployment -> Build Tools -> Gradle
    
    1.2 Activate DevTools       
    Automatic restart/reload when changes are detected in the project.
              
    1. Make project automatically     
    Settings – Build, Execution, Deployment – Compiler
    
    1. Enable Key "compiler.automake.allow.when.app.running"        
    
    -   Open Registry: Press Ctrl-Alt-Shift-/ and select "Registry"     
    -   From menu that appears, find and select the checkbox to make it active.     

2. **Remote Debug**   
    
#### Configuration: application.properties file    
Configuration sections are:

###### Application     

    app.code=1319   
    app.name=ms 
    app.title=MS    
    app.description=${app.name} is a template application   
    api.base-url=/api

###### Application port 
    
    # The embedded tomcat server start on port 8080. 
    server.port=8080
    
###### Ldap
    # The embedded tomcat server start on port 8080. 
    spring.ldap.embedded.ldif=classpath:/ldap/ms-ldap.ldif
    
    
###### Postgres

    spring.datasource.initialization-mode=always
    spring.datasource.platform=ms-db
    spring.datasource.url=jdbc:postgresql://localhost:5432/ms-db
    spring.datasource.username=user
    spring.datasource.password=password


###### Liquibase: database schema evolution
    
    spring.liquibase.enabled=true
    spring.liquibase.change-log=classpath:/liquibase/changelog-master.xml 
    
###### Swagger: API Documentation 
  
    swagger.base-url=/
    

    
###### External web services  
     ws.country.base-url=http://country.io
     ws.country.names.base-uri=/names.json