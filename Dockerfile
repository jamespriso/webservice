# Image de base : RedHat Universal Base Image 7
FROM registry.access.redhat.com/ubi7-init

ARG proxy
ENV http_proxy $proxy
ENV https_proxy $proxy

ENV no_proxy ""
ENV GRADLE_USER_HOME /home/usine-logicielle/.gradle

USER root


# Installation OpenJDK 1.8
RUN yum update; yum -y install java-11-openjdk-devel ;\
yum -y install git ;\
yum -y install sudo ;\
yum clean all; \
mkdir -p /opt/tomcat


# Creation utilisateur usine-logicielle qui executera les processus à l'interieur du conteneur
RUN groupadd usine-logicielle ;\
    useradd -s /bin/bash -g usine-logicielle -d /home/usine-logicielle usine-logicielle ;\
    mkdir -p /home/usine-logicielle ;\
    mkdir -p /home/usine-logicielle/.gradle ; \
    echo "usine-logicielle ALL= NOPASSWD:/usr/bin/gitlab-runner, /opt/tomcat/bin/startup.sh, /opt/to$
    echo 'Defaults env_keep += "http_proxy https_proxy no_proxy GRADLE_USER_HOME"' >> /etc/sudoers.d$



# Installation de Tomcat
#RUN groupadd tomcat ;\
#useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat ;\
#mkdir -p /opt/tomcat
WORKDIR /opt/tomcat

RUN yum install -y wget;\
wget https://downloads.apache.org/tomcat/tomcat-8/v8.5.56/bin/apache-tomcat-8.5.56.tar.gz -O - | tar$
chgrp -R usine-logicielle /opt/tomcat;\
chmod g+w . ;\
chmod -R g+r conf;\
chmod g+x conf;\
chmod -R g+x bin; \
chown -R usine-logicielle:usine-logicielle webapps/ work/ temp/ logs/ ;\
echo "export JAVA_HOME=/usr/lib/jvm/java-11" >> /etc/profile.d/javaenv.sh;\
echo "export JRE_HOME=/usr/lib/jvm/jre-11/" >> /etc/profile.d/javaenv.sh


#### GRADLE ###
RUN yum install unzip wget -y;\
wget https://services.gradle.org/distributions/gradle-5.3-bin.zip -P /tmp;\
unzip -d /opt/gradle /tmp/gradle-*.zip;\
echo "export GRADLE_HOME=/opt/gradle/gradle-5.3" >> /etc/profile.d/gradle.sh;\
echo "export PATH=/opt/gradle/gradle-5.3/bin:${PATH}"  >> /etc/profile.d/gradle.sh;\
source /etc/profile.d/gradle.sh

#RUN chown tomcat:tomcat /tmp/application.properties; \
#    mkdir /tmp/orpeacrm ;\
#    groupadd usine-logicielle ;\
#    useradd -s /bin/bash -g usine-logicielle -d /home/usine-logicielle usine-logicielle ;\
#    mkdir -p /home/usine_logicielle ;\
#    echo "usine-logicielle ALL= NOPASSWD:/usr/bin/gitlab-runner" > /etc/sudoers.d/usine-logicielle

#######################################################################

# Installation de nodejs
RUN curl -sL https://rpm.nodesource.com/setup_12.x | bash - ;\
yum install -y nodejs

#Installation de gitlab runner
RUN curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | ba$
yum -y install gitlab-runner ;\
yum clean all
#echo "172.30.0.113 gitlab.orpea.net" >> /etc/hosts

#COPY .gitconfig /root/.gitconfig
#######################################################################
COPY rootCASectigo.crt /etc/pki/ca-trust/source/anchors/
COPY Intermediate-Comodo-OV-SSL-Wildcard-RSA-SHA2-primary.txt /etc/pki/ca-trust/source/anchors
RUN update-ca-trust extract

COPY application.properties /tmp/application.properties
COPY gradle.properties /home/usine-logicielle/.gradle/gradle.properties


RUN chown -R usine-logicielle:usine-logicielle /home/usine-logicielle/.gradle; \
chown usine-logicielle:usine-logicielle /tmp/application.properties

#######################################################################
#  --url "https://gitlab.limengo.net" \
#  --registration-token "5NBNfZ62Vx9w9p-TXpBb" \

USER usine-logicielle
RUN sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.orpea.net" \
  --registration-token "b1fRD26rWuUx28LgeWms" \
  --executor "shell" \
  --description "docker-runner" \
  --tag-list "ms-edi-service" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"

WORKDIR /home/usine-logicielle

EXPOSE 8080
