package com.orpea.ms.controller;


import com.orpea.ms.common.util.ResponseEntityUtils;
import com.orpea.ms.model.OrganizationDto;
import com.orpea.ms.service.OrganizationService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;


@Api(tags = "Organization management")
@Slf4j
@RestController
@RequestMapping(value = "/organizations")
public class OrganizationController {

    private OrganizationService service;

    @Inject
    public OrganizationController(OrganizationService service) {
        this.service = service;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OrganizationDto>> getOrganizations() {
        log.info("[ms]: Get all organizations.");
        List<OrganizationDto> response = service.getOrganizationList();
        return ResponseEntityUtils.createResponse(response, true);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrganizationDto> getOrganizations(@PathVariable(name = "id") final String id) {
        log.info("[ms]: Get organization.");
        OrganizationDto organization = service.getOrganization(id);
        return ResponseEntityUtils.createResponse(organization, false);
    }
}
