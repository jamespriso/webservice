package com.orpea.ms.controller;


import com.orpea.ms.common.exception.http.BadRequestException;
import com.orpea.ms.common.util.ResponseEntityUtils;
import com.orpea.ms.domain.User;
import com.orpea.ms.model.UserDto;
import com.orpea.ms.service.UserService;
import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.inject.Inject;
import java.util.List;

@Api(tags = "User management")
@Slf4j
@RestController
@RequestMapping(value = UserDto.HREF)
public class UserController {

    private UserService service;

    @Inject
    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDto>> getAllUsers() {
        log.info("[ms]: Get all users.");
        List<UserDto> response = service.getAllUsers();
        return ResponseEntityUtils.createResponse(response, true);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> getUser(@PathVariable(name = "id") final String id) {
        log.info("[ms]: Get user.");
        UserDto user = service.getUser(id);
        return ResponseEntityUtils.createResponse(user, false);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> createUser(@RequestBody UserDto user, final UriComponentsBuilder builder) {
        log.info("[ms]:Create user.");
        User createdUser = service.createUser(user);
        return ResponseEntityUtils.createResponse(builder, createdUser.getId());
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> updateUser(@PathVariable(name = "id") final String id, @RequestBody UserDto user) {
        log.info("[ms]: Update user. [full update].");
        checkId(id);
        service.updateUser(id, user);
        return ResponseEntityUtils.okResponse();
    }


    @PatchMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> patchUser(@PathVariable(name = "id") final String id) {
        log.info("[ms]: Patch user. [partial update].");
        //  TODO: Implementation must respect RFC describe at https://tools.ietf.org/pdf/rfc6902.pdf
        return ResponseEntityUtils.okResponse();
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> deleteUser(@PathVariable(name = "id") final String id) {
        log.info("[ms]: Delete user.");
        service.deleteUser(id);
        return ResponseEntityUtils.okResponse();
    }


    private void checkId(final String id) {
        if (StringUtils.isBlank(id)) {
            throw new BadRequestException(String.format("Resource with ID '%s' must not be null or blank", id));
        }
    }


}
