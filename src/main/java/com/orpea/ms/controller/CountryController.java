package com.orpea.ms.controller;


import com.orpea.ms.client.CountryClient;
import com.orpea.ms.common.util.ResponseEntityUtils;
import com.orpea.ms.model.CountryNameDto;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@Api(tags = "Country API")
@Slf4j
@RestController
@RequestMapping(value = "/countries")
public class CountryController {


    private CountryClient countryClient;


    @GetMapping(value = "/names", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CountryNameDto>> getCountryNames() {
        log.info("[ms]: Get country names.");
        List<CountryNameDto> response = countryClient.getCountryNames();
        return ResponseEntityUtils.createResponse(response, true);
    }

    // Standard getters & setters
    @Inject
    public void setCountryClient(CountryClient countryClient) {
        this.countryClient = countryClient;
    }
}
