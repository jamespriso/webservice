package com.orpea.ms.model;

public enum Civility {
    M, MLLE, MME
}
