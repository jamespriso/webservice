package com.orpea.ms.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.orpea.ms.common.dto.BaseDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Objects;

import static lombok.AccessLevel.PRIVATE;

@Getter
@FieldDefaults(level = PRIVATE)
@NoArgsConstructor
public class UserDto extends BaseDto {

    public static final String HREF = "/users";
    private static final long serialVersionUID = 6950520053308158445L;
    String id;
    String firstName;
    String lastName;
    String email;
    String username;
    @JsonProperty(access = Access.WRITE_ONLY)
    String password;
    Civility civility;
    List<String> roles;


    public static UserDto newInstance() {
        return new UserDto();
    }

    public UserDto id(String id) {
        this.id = id;
        return this;
    }

    public UserDto firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserDto lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UserDto email(String email) {
        this.email = email;
        return this;
    }

    public UserDto username(String username) {
        this.username = username;
        return this;
    }

    public UserDto password(String password) {
        this.password = password;
        return this;
    }

    public UserDto civility(Civility civility) {
        this.civility = civility;
        return this;
    }

    public UserDto roles(List<String> roles) {
        this.roles = roles;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(email, userDto.email) &&
                Objects.equals(username, userDto.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, username);
    }
}
