package com.orpea.ms.model;

import com.orpea.ms.common.dto.BaseDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CountryNameDto extends BaseDto {
    private static final long serialVersionUID = -3939028717480724307L;
    private String code;
    private String name;

    private CountryNameDto(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static CountryNameDto of(String code, String name) {
        return new CountryNameDto(code, name);
    }
}
