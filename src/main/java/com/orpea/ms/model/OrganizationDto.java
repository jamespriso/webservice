package com.orpea.ms.model;

import com.orpea.ms.common.dto.BaseDto;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;


@Builder
@Getter
@Setter
public class OrganizationDto extends BaseDto {

    private static final long serialVersionUID = -5855217842053263875L;
    private String id;
    private String name;
    private String description;
    private OffsetDateTime creationDate;

}
