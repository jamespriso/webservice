package com.orpea.ms.common.exception;

import com.orpea.ms.common.exception.http.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {BadRequestException.class, IllegalArgumentException.class})
    protected ResponseEntity<Object> handleBadRequestException(RuntimeException e, WebRequest request) {
        log.error("handle bad request exception", e);
        if (e instanceof IllegalArgumentException) {
            ErrorResponse response = ErrorResponse.of(BadRequestException.HTTP_STATUS.value(), e.getMessage());
            return handleExceptionInternal(e, response, new HttpHeaders(), BadRequestException.HTTP_STATUS, request);
        } else {
            return getObjectResponseEntity((BadRequestException) e, BadRequestException.HTTP_STATUS, request);
        }

    }

    @ExceptionHandler(value = {ConflictException.class})
    protected ResponseEntity<Object> handleConflictException(RuntimeException e, WebRequest request) {
        log.error("handle conflict exception", e);
        return getObjectResponseEntity((ConflictException) e, ConflictException.HTTP_STATUS, request);
    }


    @ExceptionHandler(value = {ForbiddenException.class})
    protected ResponseEntity<Object> handleForbiddenException(RuntimeException e, WebRequest request) {
        log.error("handle forbidden exception", e);
        return getObjectResponseEntity((ForbiddenException) e, ForbiddenException.HTTP_STATUS, request);
    }


    @ExceptionHandler(value = {GatewayTimeoutException.class})
    protected ResponseEntity<Object> handleGatewayTimeoutException(RuntimeException e, WebRequest request) {
        log.error("handle timeout exception", e);
        return getObjectResponseEntity((GatewayTimeoutException) e, GatewayTimeoutException.HTTP_STATUS, request);
    }

    @ExceptionHandler(value = {ResourceNotFoundException.class})
    protected ResponseEntity<Object> handleResourceNotFoundException(RuntimeException e, WebRequest request) {
        log.error("handle resource not found exception", e);
        return getObjectResponseEntity((ResourceNotFoundException) e, ResourceNotFoundException.HTTP_STATUS, request);
    }

    @ExceptionHandler(value = {TechnicalException.class})
    protected ResponseEntity<Object> handleTechnicalException(RuntimeException e, WebRequest request) {
        log.error("handle technical exception", e);
        return getObjectResponseEntity((TechnicalException) e, TechnicalException.HTTP_STATUS, request);
    }

    @ExceptionHandler(value = {UnauthorizedException.class})
    protected ResponseEntity<Object> handleUnauthorizedException(RuntimeException e, WebRequest request) {
        log.error("handle unauthorized exception", e);
        return getObjectResponseEntity((UnauthorizedException) e, UnauthorizedException.HTTP_STATUS, request);
    }


    private ResponseEntity<Object> getObjectResponseEntity(HttpException e, HttpStatus status, WebRequest request) {
        final ErrorResponse response = ErrorResponse.of(e.getStatus(), e.getMessage());
        return handleExceptionInternal(e, response, new HttpHeaders(), status, request);
    }


}
