package com.orpea.ms.common.exception.http;

public class HttpException extends RuntimeException {

    private static final long serialVersionUID = 1226335330063988538L;
    private final int status;

    public HttpException(final int status, final String message) {
        super(message);
        this.status = status;
    }

    public HttpException(final int status, final Throwable cause) {
        super(String.valueOf(status), cause);
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}

