package com.orpea.ms.common.exception.http;

import org.springframework.http.HttpStatus;

public class TechnicalException extends HttpException {

    private static final long serialVersionUID = -6773861135313989219L;
    public static final HttpStatus HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;
    public static final int CODE = HTTP_STATUS.value();
    private Throwable cause;

    public TechnicalException(String msg) {
        super(CODE, msg);
    }

    @Override
    public synchronized Throwable getCause() {
        return cause;
    }

    public synchronized void setCause(Throwable cause) {
        this.cause = cause;
    }
}
