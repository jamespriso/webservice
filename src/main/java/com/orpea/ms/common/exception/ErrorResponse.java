package com.orpea.ms.common.exception;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;

@Getter
@EqualsAndHashCode
public class ErrorResponse implements Serializable {

    private static final long serialVersionUID = -2679773006058304507L;
    private int code;
    private String message;

    private ErrorResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ErrorResponse of(final int code, final String message) {
        return new ErrorResponse(code, message);
    }
}

