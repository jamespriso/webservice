package com.orpea.ms.common.exception.http;

import org.springframework.http.HttpStatus;

public class BadRequestException extends HttpException {

    private static final long serialVersionUID = 2615684387146687746L;
    public static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;
    public static final int CODE = HTTP_STATUS.value();
    private Throwable cause;

    public BadRequestException(String msg) {
        super(CODE, msg);
    }

    @Override
    public synchronized Throwable getCause() {
        return cause;
    }

    public synchronized void setCause(Throwable cause) {
        this.cause = cause;
    }
}

