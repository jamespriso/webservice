package com.orpea.ms.common.exception.http;

import org.springframework.http.HttpStatus;

public class UnauthorizedException extends HttpException {

    private static final long serialVersionUID = 6334186138190232439L;
    public static final HttpStatus HTTP_STATUS = HttpStatus.UNAUTHORIZED;
    public static final int CODE = HTTP_STATUS.value();
    private Throwable cause;

    public UnauthorizedException(String msg) {
        super(CODE, msg);
    }

    @Override
    public synchronized Throwable getCause() {
        return cause;
    }

    public synchronized void setCause(Throwable cause) {
        this.cause = cause;
    }
}


