package com.orpea.ms.common.exception.http;

import org.springframework.http.HttpStatus;

public class GatewayTimeoutException extends HttpException {

    private static final long serialVersionUID = 3805401147997577552L;
    public static final HttpStatus HTTP_STATUS = HttpStatus.GATEWAY_TIMEOUT;
    public static final int CODE = HTTP_STATUS.value();
    private Throwable cause;

    public GatewayTimeoutException(String msg) {
        super(CODE, msg);
    }

    @Override
    public synchronized Throwable getCause() {
        return cause;
    }

    public synchronized void setCause(Throwable cause) {
        this.cause = cause;
    }
}

