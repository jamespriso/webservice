package com.orpea.ms.common.exception.http;

import org.springframework.http.HttpStatus;

public class ForbiddenException extends HttpException {

    private static final long serialVersionUID = -1677463871129003476L;
    public static final HttpStatus HTTP_STATUS = HttpStatus.FORBIDDEN;
    public static final int CODE = HTTP_STATUS.value();
    private Throwable cause;

    public ForbiddenException(String msg) {
        super(CODE, msg);
    }

    @Override
    public synchronized Throwable getCause() {
        return cause;
    }

    public synchronized void setCause(Throwable cause) {
        this.cause = cause;
    }
}

