package com.orpea.ms.common.util;

public class DateFormatPattern {
    public static final String DD_MM_YYYY_HH_MM = "dd/MM/yyyy HH:mm";
    public static final String YYYY_MM_DD = "yyyyMMdd";
    public static final String HH_MM = "HHmm";

    private DateFormatPattern() {
        // Private no-args constructor.
    }
}
