package com.orpea.ms.common.util;

import com.orpea.ms.common.exception.http.BadRequestException;
import com.orpea.ms.common.exception.http.ForbiddenException;
import com.orpea.ms.common.exception.http.UnauthorizedException;
import com.orpea.ms.model.UserDto;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collection;
import java.util.UUID;

import static com.orpea.ms.common.util.TransactionIdHeader.TID;
import static com.orpea.ms.common.util.TransactionIdHeader.createTransactionId;
import static org.springframework.http.HttpStatus.OK;

@Slf4j
@UtilityClass
public class ResponseEntityUtils {

    public static <T> ResponseEntity<T> createResponse(final T body, final boolean isBodyCollectionType) {
        if (isBodyCollectionType) {
            return new ResponseEntity<>(body, MsCollectionUtils.isNotEmpty((Collection) body) ? OK : HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(body, body != null ? OK : HttpStatus.NOT_FOUND);
    }


    public static <T> ResponseEntity<T> createdResponse() {
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    public static <T> ResponseEntity<T> createResponse(UriComponentsBuilder builder, final String id) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path(String.format("%s/{id}", "/api/" + UserDto.HREF)).buildAndExpand(id).toUri());
        String uuid = UUID.randomUUID().toString();
        headers.set(TID, uuid);
        MDC.put(TID, createTransactionId(uuid));
        log.debug("Header value Location {}", headers.getLocation());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    public static <T> ResponseEntity<T> okResponse() {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public static void checkResponseStatus(ResponseEntity<?> response) {
        HttpStatus status = response.getStatusCode();
        if (status == HttpStatus.UNAUTHORIZED) {
            throw new UnauthorizedException("Authentication is required to access the resource.");
        }
        if (status == HttpStatus.FORBIDDEN) {
            throw new ForbiddenException("Insufficient access rights to access the resource.");
        }
        if (status == HttpStatus.BAD_REQUEST) {
            throw new BadRequestException("Resource not found.");
        }
    }
}
