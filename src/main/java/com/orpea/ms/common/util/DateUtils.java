package com.orpea.ms.common.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import static com.google.common.base.Preconditions.checkArgument;
import static com.orpea.ms.common.util.MsStringUtils.*;

@Slf4j
@UtilityClass
public final class DateUtils {

    public static String formatDate(OffsetDateTime dateTime, final String pattern) {
        if (dateTime == null) {
            return null;
        }
        checkArgument(isNotBlank(pattern), "pattern must not be null");
        return dateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

}
