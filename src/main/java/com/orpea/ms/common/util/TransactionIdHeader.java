package com.orpea.ms.common.util;


import lombok.experimental.UtilityClass;

@UtilityClass
public class TransactionIdHeader {

    public static final String TID = "MS_TRANSACTION_ID";
    public static final String TID_PREFIX = "TID=";

    public static String createTransactionId(String uuid) {
        return TID_PREFIX + uuid;
    }
}
