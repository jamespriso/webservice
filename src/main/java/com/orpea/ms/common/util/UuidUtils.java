package com.orpea.ms.common.util;


import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class UuidUtils {

    public static final int UUID_STRING_LENGTH = 36;
    public static String createUUID() {
        return UUID.randomUUID().toString();
    }
}
