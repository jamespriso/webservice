package com.orpea.ms.common.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@UtilityClass
public final class MsStringUtils extends org.apache.commons.lang3.StringUtils {

}
