package com.orpea.ms.common.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.experimental.UtilityClass;

import java.io.IOException;
import java.io.InputStream;

@UtilityClass
public class JsonUtils {

    public static final ObjectMapper MAPPER = initObjectMapper();


    public static ObjectMapper initObjectMapper() {
        final ObjectMapper mapper = new ObjectMapper();
        return mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true)
                .configure(SerializationFeature.INDENT_OUTPUT, true)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
    }

    public static String toJson(final Object object) {
        try {
            return MAPPER.writeValueAsString(object);
        } catch (IOException e) {
            String msg = "Error convert to json from object " + object.toString();
            throw new IllegalStateException(msg, e);
        }
    }

    public static <T> T fromJson(final InputStream input, final Class<T> clazz) {
        try {
            return MAPPER.readValue(input, clazz);
        } catch (IOException e) {
            String msg = "Error converting from json {" + input + "} " + "to object " + clazz.getName();
            throw new IllegalStateException(msg, e);
        }
    }

    public static <T> T fromJson(final String input, final Class<T> clazz) {
        try {
            return MAPPER.readValue(input, clazz);
        } catch (IOException e) {
            String msg = "Error converting from json {" + input + "} " + "to object " + clazz.getName();
            throw new IllegalStateException(msg, e);
        }
    }

}
