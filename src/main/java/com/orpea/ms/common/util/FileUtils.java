package com.orpea.ms.common.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Slf4j
@UtilityClass
public final class FileUtils {

    public static String readJsonFile(final String path) {
        String content = null;
        try {
            InputStream inputStream = ResourceUtils.getURL("classpath:" + path).openStream();
            content = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            log.error("Error occurred while reading json file", e);
        }
        return content;
    }


}
