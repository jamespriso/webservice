package com.orpea.ms.common.web.auth;

public class Authorization {

    private final AuthorizationType type;

    public Authorization(AuthorizationType type) {
        this.type = type;
    }

    public AuthorizationType getType() {
        return type;
    }

}
