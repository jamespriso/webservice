package com.orpea.ms.common.web.auth;

public enum AuthorizationType { BASIC_AUTH, OAUTH2, BEARER_TOKEN }