package com.orpea.ms.common.web.auth;

import lombok.Getter;

import java.util.Base64;

@Getter
public class BasicAuth extends Authorization {

    private final String login;
    private final String password;

    public BasicAuth(String login, String password) {
        super(AuthorizationType.BASIC_AUTH);
        this.login = login;
        this.password = password;
    }

    public String getBasicAuthorizationToken() {
        final String plainCredentials = this.login + ":" + this.password;
        String credentials = Base64.getEncoder().encodeToString(plainCredentials.getBytes());
        return "Basic " + credentials;
    }


}
