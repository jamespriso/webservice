package com.orpea.ms.common.web.auth;

public class BearerToken extends Authorization {

    private final String token;

    public BearerToken(final String token) {
        super(AuthorizationType.BEARER_TOKEN);
        this.token = token;
    }

    public String getBearerToken() {
        return "Bearer " + token;
    }

}
