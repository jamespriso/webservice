package com.orpea.ms.common.web.auth;

public class OAuth2 extends Authorization {
    public OAuth2() {
        super(AuthorizationType.OAUTH2);
    }
}
