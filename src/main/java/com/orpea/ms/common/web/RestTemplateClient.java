package com.orpea.ms.common.web;


import com.orpea.ms.common.web.auth.Authorization;
import com.orpea.ms.common.web.auth.AuthorizationType;
import com.orpea.ms.common.web.auth.BasicAuth;
import com.orpea.ms.common.web.auth.BearerToken;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Slf4j
@NoArgsConstructor
public final class RestTemplateClient {

    private Authorization authorization;
    private HttpHeaders headers = new HttpHeaders();
    private MultiValueMap<String, String> queryParams;
    private Map<String, Object> uriParams;
    // Url = baseUrl + baseUri
    private String url;


    public <T> ResponseEntity<T> request(final HttpMethod httpMethod, Object body, Class<T> responseType) {
        ResponseEntity<T> response;
        URI uriComponent = null;
        try {
            RestTemplate template = new RestTemplate();
            setAuthorization(authorization);
            boolean isGetRequest = (body == null) && (httpMethod == HttpMethod.POST || httpMethod == HttpMethod.PUT || httpMethod == HttpMethod.DELETE);
            HttpEntity<T> requestEntity = isGetRequest ? new HttpEntity<>(headers) : new HttpEntity(body, headers);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            UriComponents components = buildUriComponents(queryParams, uriParams, builder);

            uriComponent = components.toUri();
            response = template.exchange(uriComponent, httpMethod, requestEntity, responseType);
            log.info("The url of RESTful web service invoked is: '{}'.", uriComponent);
        } catch (RestClientException e) {
            log.error(String.format("Exception occurred while calling method '%s' at '%s'.", httpMethod.toString(), uriComponent), e);
            throw e;
        }
        return response;
    }

    private UriComponents buildUriComponents(MultiValueMap<String, String> queryParams, Map<String, Object> uriParams, UriComponentsBuilder builder) {
        UriComponents components = null;
        if (queryParams == null && uriParams == null) {
            return builder.build();
        }
        if (queryParams != null && uriParams != null) {
            components = builder.queryParams(queryParams).buildAndExpand(uriParams).encode();
        } else {
            if (uriParams != null) {
                components = builder.buildAndExpand(uriParams);
            }
            if (queryParams != null) {
                components = builder.queryParams(queryParams).build().encode();
            }
        }
        return components;
    }

    private void setAuthorization(final Authorization authorization) {
        String value = EMPTY;
        if (authorization != null) {
            if (authorization.getType() == AuthorizationType.BEARER_TOKEN) {
                value = ((BearerToken) authorization).getBearerToken();
            }
            if (authorization.getType() == AuthorizationType.BASIC_AUTH) {
                value = ((BasicAuth) authorization).getBasicAuthorizationToken();
            }
            if (authorization.getType() == AuthorizationType.OAUTH2) {
                throw new NotImplementedException(String.format("Authorization type « %s » is not yet implemented.", authorization.getType()));
            }
        }
        this.headers.add(AUTHORIZATION, value);
    }

    public static class Builder<T> {

        private RestTemplateClient template = new RestTemplateClient();

        public Builder<T> withAuthorization(Authorization authorization) {
            template.authorization = authorization;
            return this;
        }

        public Builder<T> withHeaders(HttpHeaders headers) {
            template.headers = headers;
            return this;
        }

        public Builder<T> uri(String url) {
            template.url = url;
            return this;
        }

        public Builder<T> queryParams(MultiValueMap<String, String> parameters) {
            template.queryParams = parameters;
            return this;
        }

        public Builder<T> uriParams(Map<String, Object> uriParameters) {
            template.uriParams = uriParameters;
            return this;
        }

        public ResponseEntity<T> get(Class<T> responseType) {
            return template.request(HttpMethod.GET, null, responseType);
        }

        public ResponseEntity<T> post(Object body, Class<T> responseType) {
            return template.request(HttpMethod.POST, body, responseType);
        }

        public ResponseEntity<T> put(Object body, Class<T> responseType) {
            return template.request(HttpMethod.PUT, body, responseType);
        }

        public ResponseEntity<T> delete(Class<T> responseType) {
            return template.request(HttpMethod.DELETE, null, responseType);
        }
    }

}

