package com.orpea.ms.client;


import com.orpea.ms.common.web.RestTemplateClient;
import com.orpea.ms.model.CountryNameDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
public class CountryClient {

    @Value("${ws.country.base-url}")
    private String baseUrl;

    @Value("${ws.country.names.base-uri}")
    private String baseUri;

    public List<CountryNameDto> getCountryNames() {
        final String url = baseUrl + baseUri;
        try {
            ResponseEntity<Map<String, String>> response = new RestTemplateClient.Builder()
                    .uri(url)
                    .get(Map.class);

            Map<String, String> map = response.getBody();
            return toCountryNameList(map);

        } catch (RestClientException e) {
            log.error("Exception occurred when calling WS Country located at'{}'.", url);
        }
        return Collections.emptyList();
    }

    private static List<CountryNameDto> toCountryNameList(Map<String, String> map) {
        return map.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .map(e -> CountryNameDto.of(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

}
