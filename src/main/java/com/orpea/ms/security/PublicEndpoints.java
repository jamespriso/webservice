package com.orpea.ms.security;

public final class PublicEndpoints {

    public static final String AUTH_URL = "/login";

    /**
     * Endpoints Actuator.
     */
    public static final String MANAGEMENT_HEALTH = "/management/health";
    public static final String MANAGEMENT_INFO = "/management/info";

    private PublicEndpoints() {
        // Private no-args constructor.
    }
}
