package com.orpea.ms.security;

public final class AuthorityConstants {
    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
    public static final String ANONYMOUS = "ANONYMOUS";

    private AuthorityConstants() {
        // Private no-args constructor.
    }
}
