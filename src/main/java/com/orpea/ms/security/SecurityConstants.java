package com.orpea.ms.security;

import static org.springframework.http.HttpHeaders.*;

public final class SecurityConstants {

    public static final String SECRET = "confidential";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";

    public static final String AUTHORIZATION_HEADER = AUTHORIZATION;
    public static final String ACCESS_CONTROL_ALLOW_ORIGIN_HEADER = ACCESS_CONTROL_ALLOW_ORIGIN;
    public static final String ACCESS_CONTROL_ALLOW_HEADERS_HEADER = ACCESS_CONTROL_ALLOW_HEADERS;
    public static final String ACCESS_CONTROL_EXPOSE_HEADERS_HEADER = ACCESS_CONTROL_EXPOSE_HEADERS;

    private SecurityConstants() {
        // Private no-args constructor.
    }
}
