package com.orpea.ms.security.jwt.filter;

import com.orpea.ms.security.jwt.JWTHelper;
import io.jsonwebtoken.Claims;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import static com.orpea.ms.security.SecurityConstants.*;

public class JWTAuthorizationFilter extends OncePerRequestFilter {

    public static final String AUTHORITIES_KEY = "roles";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        response.addHeader(ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        response.setHeader(ACCESS_CONTROL_ALLOW_HEADERS_HEADER, "Origin, Accept, Content-Type, Key, X-Requested-With, Access-Control-Requested-Headers, Access-Control-Requested-Method, Authorization");
        response.setHeader(ACCESS_CONTROL_EXPOSE_HEADERS_HEADER, "Access-Control-Allow-Origin,Access-Control-Allow-Credentials, Authorization");

        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
        }
        String token = request.getHeader(AUTHORIZATION_HEADER);
        if (token == null || !token.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token) {
        final Claims claims = JWTHelper.getClaims(token);
        final String username = claims.getSubject();
        final Collection<GrantedAuthority> authorities = new ArrayList<>();

        ArrayList<Map<String, String>> roles = (ArrayList<Map<String, String>>) claims.get(AUTHORITIES_KEY);
        roles.stream().forEach(role ->
                authorities.add(new SimpleGrantedAuthority(role.get("authority")))
        );
        return new UsernamePasswordAuthenticationToken(username, null, authorities);
    }
}
