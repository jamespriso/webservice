package com.orpea.ms.security.jwt.filter;

import com.orpea.ms.common.exception.http.UnauthorizedException;
import com.orpea.ms.common.util.JsonUtils;
import com.orpea.ms.model.AuthRequest;
import com.orpea.ms.security.jwt.JWTHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.orpea.ms.security.SecurityConstants.AUTHORIZATION_HEADER;
import static com.orpea.ms.security.SecurityConstants.TOKEN_PREFIX;

@Slf4j
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        super();
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        AuthRequest userAuth;
        try {
            userAuth = JsonUtils.fromJson(request.getInputStream(), AuthRequest.class);
        } catch (IOException e) {
            log.error("Exception occurred while retrieving information for user authentication.", e);
            throw new UnauthorizedException("Exception occurred while retrieving information for user authentication.");
        }
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userAuth.getUsername(), userAuth.getPassword());
        return authenticationManager.authenticate(authentication);
    }


    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) {
        User user = (User) authResult.getPrincipal();
        String token = JWTHelper.createToken(user);
        response.addHeader(AUTHORIZATION_HEADER, TOKEN_PREFIX + token);
    }


}
