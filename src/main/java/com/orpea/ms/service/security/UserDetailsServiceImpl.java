package com.orpea.ms.service.security;

import com.orpea.ms.model.UserDto;
import com.orpea.ms.service.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserService service;

    @Inject
    public UserDetailsServiceImpl(UserService service) {
        this.service = service;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        UserDto user = service.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("Username '%s' is not found in DB.", username));
        }
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().stream().forEach(role ->
                authorities.add(new SimpleGrantedAuthority(role))
        );
        return new User(user.getUsername(), user.getPassword(), authorities);
    }
}
