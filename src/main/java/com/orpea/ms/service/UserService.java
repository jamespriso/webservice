package com.orpea.ms.service;


import com.orpea.ms.common.exception.http.ResourceNotFoundException;
import com.orpea.ms.common.util.MsCollectionUtils;
import com.orpea.ms.domain.Role;
import com.orpea.ms.domain.User;
import com.orpea.ms.model.UserDto;
import com.orpea.ms.repository.UserRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private UserRepository repository;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Inject
    public UserService(UserRepository repository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.repository = repository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public List<UserDto> getAllUsers() {
        List<User> userList = repository.findAll();
        return userList.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Cacheable(value = "users", key = "#id")
    public UserDto getUser(final String id) {
        Optional<User> optional = repository.findById(id);
        if (!optional.isPresent()) {
            throw new ResourceNotFoundException(String.format("User with ID '%s' does not exist ", id));
        }
        return toDto(optional.get());
    }

    public User createUser(UserDto user) {
        return repository.save(toEntity(user));
    }

    @CachePut(value = "users", key = "#id")
    public UserDto updateUser(final String id, UserDto u) {
        Optional<User> optional = repository.findById(id);
        if (!optional.isPresent()) {
            throw new ResourceNotFoundException(String.format("User with update with ID '%s' does not exist in database !", u.getId()));
        }
        User entity = optional.get();
        entity.setId(id);
        entity.setFirstName(u.getFirstName());
        entity.setLastName(u.getLastName());
        entity.setEmail(u.getEmail());
        entity.setUsername(u.getUsername());
        entity.setCivility(u.getCivility());
        User updatedUser = repository.save(entity);
        return toDto(updatedUser);
    }


    @CacheEvict(value = "users", key = "#id")
    public void deleteUser(final String id) {
        Optional<User> optional = repository.findById(id);
        if (!optional.isPresent()) {
            throw new ResourceNotFoundException(String.format("User with ID '%s' does not exist ", id));
        }
        repository.deleteById(id);
    }

    private User toEntity(UserDto user) {
        User entity = User.newInstance();
        entity.setFirstName(user.getFirstName());
        entity.setLastName(user.getLastName());
        entity.setUsername(user.getUsername());
        entity.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        entity.setEmail(user.getEmail());
        entity.setCivility(user.getCivility());
        return entity;
    }

    private UserDto toDto(User user) {
        if (user == null) {
            return null;
        }
        return UserDto.newInstance()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .username(user.getUsername())
                .password(user.getPassword())
                .civility(user.getCivility())
                .roles(MsCollectionUtils.emptyIfNull(user.getRoles()).stream().map(Role::getName).collect(Collectors.toList()));
    }

    public UserDto findByUsername(final String username) {
        User applicationUser = repository.findByUsername(username);
        return toDto(applicationUser);
    }

    public User findUser(final String username) {
        return repository.findByUsername(username);
    }
}
