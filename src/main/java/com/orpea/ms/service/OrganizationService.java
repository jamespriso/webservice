package com.orpea.ms.service;

import com.orpea.ms.common.exception.http.ResourceNotFoundException;
import com.orpea.ms.common.util.UuidUtils;
import com.orpea.ms.model.OrganizationDto;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class OrganizationService {

    protected static final List<OrganizationDto> ORGANIZATIONS = Arrays.asList(
            OrganizationDto.builder()
                    .id(UuidUtils.createUUID())
                    .name("Coca-Cola Company")
                    .description("Coca-Cola description")
                    .creationDate(OffsetDateTime.now())
                    .build(),

            OrganizationDto.builder()
                    .id(UuidUtils.createUUID())
                    .name("IT Solutions")
                    .description("IT Solutions description")
                    .creationDate(OffsetDateTime.now())
                    .build()
    );

    public List<OrganizationDto> getOrganizationList() {
        return ORGANIZATIONS;
    }


    @Cacheable(value = "organizations", key = "#id")
    public OrganizationDto getOrganization(final String id) {
        Optional<OrganizationDto> optional = getOrganizationList().stream().filter(org -> org.getId().equalsIgnoreCase(id)).findAny();
        if (!optional.isPresent()) {
            throw new ResourceNotFoundException(String.format("Organization with ID '%s' does not exist ", id));
        }
       return optional.get();
    }
}
