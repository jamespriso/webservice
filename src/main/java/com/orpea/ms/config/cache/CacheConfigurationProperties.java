package com.orpea.ms.config.cache;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
@ConfigurationProperties(prefix = "cache.redis")
public class CacheConfigurationProperties {
    // TTL for all defined caches
    private long ttl;
    private int port;
    private String host;
    // Specific TTL per cache
    private Map<String, Long> expirationMap = new HashMap<>();
}
