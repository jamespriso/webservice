package com.orpea.ms.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.ServletContext;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${swagger.base-url}")
    private String swaggerBaseUrl;

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Microservice API")
                .description("All APIs can call for MS application")
                .termsOfServiceUrl("")
                .version("01.00.00")
                .contact(new Contact("Orpea MS Team", "", "r.veyrin-forrer@synelience.com"))
                .build();
    }

    @Bean
    public Docket api(ServletContext servletContext) {
        return new Docket(DocumentationType.SWAGGER_2)
                .pathProvider(new RelativePathProvider(servletContext) {
                    @Override
                    public String getApplicationBasePath() {
                        return swaggerBaseUrl;
                    }
                })
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.orpea.ms.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }


}
