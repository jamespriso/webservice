package com.orpea.ms;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerTypePredicate;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableCaching
@SpringBootApplication
public class MsApplication implements WebMvcConfigurer {

    @Value("${api.base-url}")
    private String apiBaseUrl;

    public static void main(String[] args) {
        SpringApplication.run(MsApplication.class, args);
    }

    @Override
    public void configurePathMatch(PathMatchConfigurer config) {
        config.addPathPrefix(apiBaseUrl, HandlerTypePredicate.forAnnotation(RestController.class));
    }


}
