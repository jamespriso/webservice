package com.orpea.ms.service;

import com.orpea.ms.builder.UserBuilder;
import com.orpea.ms.domain.User;
import com.orpea.ms.model.UserDto;
import com.orpea.ms.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ActiveProfiles("dev")
@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestPropertySource(locations = "classpath:test.properties")
class UserServiceTest {

    @Mock
    private UserRepository repository;

    @InjectMocks
    private UserService service;

    @Test
    void getUserTest() {
        // Given
        UserDto userExpected = UserBuilder.createUserDto();
        Optional<User> optionalUser = UserBuilder.createOptionalUser();
        when(repository.findById(any(String.class))).thenReturn(optionalUser);
        // When
        UserDto actualUser = service.getUser(anyString());
        // Then
        assertTrue(userExpected.equals(actualUser));
    }


}