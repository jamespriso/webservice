package com.orpea.ms.common.util;

import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static com.orpea.ms.common.util.DateFormatPattern.*;
import static com.orpea.ms.common.util.DateUtils.formatDate;
import static org.junit.jupiter.api.Assertions.*;

class DateUtilsTest {

    OffsetDateTime dateTime = OffsetDateTime.of(
            2020,
            4,
            6,
            22,
            30,
            10,
            77,
            ZoneOffset.UTC);

    @Test
    void formatDateTest() {
        // Given:
        String pattern = DD_MM_YYYY_HH_MM;
        // When:
        final String actual = formatDate(dateTime, pattern);
        // Then:
        assertTrue(actual.equalsIgnoreCase("06/04/2020 22:30"));
    }


    @Test
    public void getDate() {
        // Given: cf. params
        // When:
        final String actual = formatDate(dateTime, YYYY_MM_DD);
        // Then:
        assertTrue("20200406".equalsIgnoreCase(actual));
    }

    @Test
    public void getTime() {
        // Given: cf. params
        // When:
        final String actual = formatDate(dateTime, HH_MM);
        // Then:
        assertTrue("2230".equalsIgnoreCase(actual));
    }
}