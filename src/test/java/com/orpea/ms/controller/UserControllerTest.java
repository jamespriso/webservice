package com.orpea.ms.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.orpea.ms.builder.UserBuilder;
import com.orpea.ms.domain.User;
import com.orpea.ms.model.UserDto;
import com.orpea.ms.service.UserService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.inject.Inject;
import java.util.List;

import static com.orpea.ms.common.util.JsonUtils.MAPPER;
import static com.orpea.ms.common.util.UuidUtils.UUID_STRING_LENGTH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@Disabled // TODO: AccessDeniedException must be handled.
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {

    public static final String API_USERS = "/api/users";

    @MockBean
    private UserService service;

    @Inject
    private TestRestTemplate template;

    @Test
    public void getAllUsersTest() {
        // given:
        List<UserDto> expectedList = UserBuilder.createUserDtoList();
        given(service.getAllUsers()).willReturn(expectedList);

        // when:
        ResponseEntity<JsonNode> response = template.getForEntity(API_USERS, JsonNode.class);
        List<UserDto> actualList = toUserDtoList(response.getBody());

        // then:
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actualList).isEqualTo(expectedList);
    }

    private List<UserDto> toUserDtoList(JsonNode fromValue) {
        return MAPPER.convertValue(fromValue, new TypeReference<>() {
        });
    }

    @Test
    public void getUserTest() {
        // given:
        UserDto expectedUser = UserBuilder.createUserDto();
        given(service.getUser(anyString())).willReturn(expectedUser);

        // when:
        ResponseEntity<UserDto> response = template.getForEntity(API_USERS + "/{id}", UserDto.class, 1);

        // then:
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(expectedUser);
    }

    @Test
    public void createUserTest() {
        // given:
        User userToCreate = UserBuilder.createUser();
        given(service.createUser(any(UserDto.class))).willReturn(userToCreate);

        // when
        ResponseEntity<UserDto> responseEntity = template.postForEntity(API_USERS, UserBuilder.createUserDto(), UserDto.class);

        // then
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        String location = responseEntity.getHeaders().getLocation().toString();
        String id = location.replaceAll(".*/", "");
        assertThat(id.length()).isEqualTo(UUID_STRING_LENGTH);
        assertThat(location).contains(API_USERS);
    }


}