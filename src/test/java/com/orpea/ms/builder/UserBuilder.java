package com.orpea.ms.builder;

import com.orpea.ms.domain.Role;
import com.orpea.ms.domain.User;
import com.orpea.ms.model.Civility;
import com.orpea.ms.model.UserDto;
import com.orpea.ms.security.AuthorityConstants;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.orpea.ms.common.util.UuidUtils.createUUID;

public class UserBuilder {

    public static User createUser() {
        User user = new User();
        user.setId(createUUID());
        user.setFirstName("Hicham");
        user.setLastName("BOUCHEMA");
        user.setEmail("h.bouchema-ext@orpea.net");
        user.setUsername("sbouchema");
        user.setCivility(Civility.M);
        user.setRoles(Arrays.asList(new Role(AuthorityConstants.ADMIN)));
        return user;
    }

    public static UserDto createUserDto() {
        return toDto(createUser());
    }

    public static Optional<User> createOptionalUser() {
        return Optional.ofNullable(createUser());
    }

    public static UserDto createAnotherUserDto() {
        return UserDto.newInstance()
                .id(createUUID())
                .firstName("Sophie")
                .lastName("VATIER")
                .email("s.vatier-ext@orpea.net")
                .username("svatier")
                .civility(Civility.MME);
    }

    public static List<UserDto> createUserDtoList() {
        return Arrays.asList(createUserDto(), createAnotherUserDto());
    }

    private static UserDto toDto(User user) {
        return UserDto.newInstance()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .username(user.getUsername())
                .email(user.getEmail())
                .civility(user.getCivility());
    }
}
