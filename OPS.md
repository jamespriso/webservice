#### Docker

##### 1. Build image from Dockerfile

    $ docker build -t ms-app .
    Or        
    $ docker build -f Dockerfile -t ms-app .
    
##### 2.  Run a container

`$ docker run ms-app -p 8080:8080`

##### 3.  Run image on detached mode

 ```
$ docker run -d ms-app -p 8080:8080
$ docker run -d ms-app -p 8080:8080 --name ms-app
 ```

##### 4. Show container logs

 ```
<CONTAINER_ID>: Use 'docker ps' command line to obtain  <CONTAINER_ID>|<CONTAINER_NAME>. 
$ docker logs [<CONTAINER_ID>|<CONTAINER_NAME>]

# View real time logging using following command:
$ docker logs [<CONTAINER_ID>|<CONTAINER_NAME>] -f

 ```

##### 5. Connect / Attach to your container interactive shell

`$ docker exec -it ms-app /bin/bash`

##### 4. Check application

    curl --location --request GET 'http://localhost:8080/api/users/ca6a9831-4eca-464a-a4ab-88a19d3e07c0' \
    --header 'Content-Type: application/json' \
    --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhYmVqaSIsImV4cCI6MTU5NTUwNTkzMCwicm9sZXMiOlt7ImF1dGhvcml0eSI6IlVTRVIifV19.Y2G-_hjS07HXYOzFH9KHdCWi2BOXG4cWByz-lAu8FJs'


#### Docker compose    
##### Running application and depend services using docker compose (locally) 
 
    $ docker-compose -f  docker-compose-all-without-app.yml up -d                  
    # NB: application must be packaged using local profile
    
##### Stop (& destroy) all using docker compose        
    $ docker-compose -f  docker-compose-all-without-app.yml down 

##### Start / Stop specific using docker compose        
    $ docker-compose -f docker-compose-all-without-app.yml start ms-ldap 
    $ docker-compose -f docker-compose-all-without-app.yml stop ms-ldap 


##### Running separatly services    

    $ docker-compose -f ldap.yml up -d        
    $ docker-compose -f postgres.yml up -d        
    $ docker-compose -f redis.yml up -d     
    $ docker-compose -f sonar.yml up -d       

##### List all running containers
`$ docker ps`

##### Delete all stopped containers
`$ docker rm $(docker ps -aq)`



#### Monitoring endpoints 
##### Monitoring  - supervision

    # Health
    $ curl localhost:8080/actuator/health
    
    # Info
    $ curl localhost:8080/actuator/info

